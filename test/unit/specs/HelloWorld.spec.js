import Vue from 'vue'
import Dashboard from '@/components/Dashboard'

describe('Dashboard.vue', () => {
  let vm
  beforeEach('should render correct contents', () => {
    const Constructor = Vue.extend(Dashboard)
    vm = new Constructor().$mount()
  })
  it('should all functions declared', () => {
    expect(vm.insert).to.be.a('function')
    expect(vm.edit).to.be.a('function')
    expect(vm.remove).to.be.a('function')
  })
  it('should insert in arrayList', () => {
    let item = { description: 'Mikael Hadler', editMode: false }
    vm.insert(item)
    expect(vm.listTodos.lenght).to.be.a(1)
  })
})
